import glob
import numpy as np
import os
import pandas as pd
import pickle as pkl
import sys
#import matplotlib.pyplot as plt

from sklearn.utils import shuffle
from sklearn.model_selection import GroupKFold, KFold,  cross_val_score
from sklearn.ensemble import RandomForestClassifier

import MMrasterio
import Module_ProcessFiles as MMpf
import Module_load_features as MMfeat
import Module_Threshold_Methods as MMthresh


path = '/home/giffards/Documents/AvalancheProject/'
path_data = path + 'Data/Suisse/'
path_process = path + 'InputProcessing/Suisse/'

tuile = '32TMS'
orbit = 'A15' # 'D66' # for 32TMS

set_of_features = ['VV', 'VVOld', 'VVSummer', 'VH', 'VHOld', 'VHSummer', 'Slope', 'DEM', 'VVmean', 'VHmean',
                   'Slopemean','Aspect', 'Treecover']  # 'Watercover', 'Glaciercover'
min_struct_to_keep = None # can be np.ones(4, 4) SAR min structure
min_struct_to_keep_GT = np.ones((4, 4)) # SPOT min structure

folder_data = path_data + 'SAR/' + orbit + '/' + tuile + '/'
file_insee_tif = path_process + tuile + '/insee_zone_map_new.tif'
file_mask = path_process + tuile + '/'+ tuile + '_region_mask.tif'

folder_depots = path_process + tuile + '/Depots_refmoy/' # folder of the existing depots files of type ' '*' + orbit_large + '*' + date_current + '*' + 'VH' + '*'
folder_process = path + 'InputProcessing/Suisse/'+ tuile + '/3bands_log_tifs/' + orbit + '/' # path where the 3bands tiffs are of type tuile + '_' + orbit_large +
                                                                                                # '_RGB_VH_' + date_old + '_' + date_current + '_'+  'meanref' + '_log10.tif'

folder_saving_results = './results_32TMS/'

file_slope = folder_data + 'Aux/' + tuile + '_PENTE.tif'
file_DEM = folder_data + 'Aux/' + tuile + '_DEM.tif'
file_aspect = folder_data + 'Aux/' + tuile + '_EXPO.tif'
file_treecover = folder_data + 'Aux/' + tuile + '_TREECOVER.tif'
file_watercover = folder_data + 'Aux/' + tuile + '_WATERCOVER.tif'
file_glaciercover = folder_data + 'Aux/' + tuile + '_GLACIERCOVER.tif'

########################################################################################
if orbit == 'D66':
    orbit_large = 'DES_066'
    dates_current = ['20180128', '20180122', '20180116', '20180110']
    dates_old = ['20180122', '20180116', '20180110', '20180104']
elif orbit == 'A15':
    orbit_large = 'ASC_015'
    dates_current = ['20180130', '20180124', '20180118', '20180112']
    dates_old = ['20180124', '20180118', '20180112', '20180106']

folder_learning = folder_saving_results + 'RF_model/' # path for where the machine learning will be saved
folder_saving_img_positives = folder_saving_results + 'imgs_for_positives_VH' +'_' + orbit + '/'
folder_saving_img_negatives = folder_saving_results + 'imgs_for_negatives_VH' +'_' + orbit + '/'
MMpf.MakeDir(folder_saving_img_positives)
MMpf.MakeDir(folder_saving_img_negatives)


band_features = {'VV': 2, 'VVOld': 1, 'VVSummer': 3,
                 'VH': 2, 'VHOld': 1, 'VHSummer': 3,
                 'Slope': 1, 'DEM': 1, 'Aspect': 1,
                 'VVmean': 2, 'VHmean': 2, 'Slopemean': 1,
                 'Glaciercover': 1, 'Treecover': 1, 'Watercover': 1}

print('orbit: ' + orbit)

flag_phase1_2 = False
if flag_phase1_2:
    for date_current, date_old in zip(dates_current, dates_old):
        try:
            file_threshold = glob.glob(folder_depots + '*' + orbit_large + '*' + date_current + '*' + 'VH' + '*')[0]
        except IndexError:
            sys.exit('Error! no folder or file found:  ' + folder_depots + '*' + orbit_large + '*' + date_current + '*' + 'VH' + '*')


        print(date_current)
        print('   phase 1: tif images of positive and negative samples starting...')
        img_thresh_noinsee = MMrasterio.open_tif(file_threshold)
        crs = MMrasterio.get_tiff_crs(img_thresh_noinsee)
        transform = MMrasterio.get_tiff_transform(img_thresh_noinsee)
        img_thresh_noinsee = img_thresh_noinsee.read()[0]
        img_thresh_noinsee = MMthresh.remove_samples_outside_mask(img_thresh_noinsee, file_mask)
        img_thresh_insee, bla = MMthresh.separate_insee_and_no_insee_detections(img_thresh_noinsee, file_insee_tif)
        bla=[]
        img_thresh_adj = MMthresh.get_adjascent_zones_thresholded(img_thresh_insee, img_thresh_noinsee,
                                                                  min_struct_to_keep=min_struct_to_keep,
                                                                  min_struct_to_keep_GT=min_struct_to_keep_GT)
        img_thresh_insee = []
        img_thresh_noinsee_diff = MMthresh.get_img_diff_of_adjascent_zones(img_thresh_noinsee,
                                                                              img_thresh_adj=img_thresh_adj, file_img_thresh_adj=None,
                                                                              min_struct_to_keep=min_struct_to_keep)

        file_threshold_positive = folder_saving_img_positives + date_current + '.tif'
        file_threshold_negative = folder_saving_img_negatives + date_current + '.tif'

        MMrasterio.save_array_to_tiff(img_thresh_adj, transform, file_threshold_positive, crs)
        img_thresh_adj = []
        MMrasterio.save_array_to_tiff(img_thresh_noinsee_diff, transform, file_threshold_negative, crs)
        img_thresh_noinsee_diff = []
        print('   phase 1 done.')

        print('   phase 2 : writing features starting...')

        file_name_3bands_VH = folder_process + tuile + '_' + orbit_large + '_RGB_VH_' + date_old + '_' + date_current + '_'+ \
                     'meanref' + '_log10.tif'
        file_name_3bands_VV = folder_process + tuile + '_' + orbit_large + '_RGB_VV_' + date_old + '_' + date_current + '_'+ \
                     'meanref' + '_log10.tif'


        img_positives, crs, transform = MMrasterio.open_tif_crs_transform(file_threshold_positive)
        img_positives = img_positives.read()[0]

        #img_insee = MMrasterio.read_tif_to_np_array(file_insee_tif)
         ## get indexes of positive and negative samples
        height = img_positives.shape[0]
        width = img_positives.shape[1]


        df = MMthresh.get_df_indices_nonzero_from_tif(tif_img=img_positives, tif_file=None,
                                                          saving_file=folder_saving_img_positives + date_current + '_indices.csv')
        img_positives = []
        df_neg = MMthresh.get_df_indices_nonzero_from_tif(tif_img=None, tif_file=file_threshold_negative,
                                                          saving_file=folder_saving_img_negatives + date_current + '_indices.csv')

        if len(df_neg) < len(df):
            df = df.sample(frac=1)[:len(df_neg)]
            df.index = range(len(df))
        csv_features_positives_file = folder_saving_img_positives + date_current + '_features_3means_covers.csv'
        csv_features_negatives_file = folder_saving_img_negatives + date_current + '_features_3means_covers.csv'
        ## get the features
        name_file_features = {'VV':file_name_3bands_VV, 'VVOld':file_name_3bands_VV, 'VVSummer': file_name_3bands_VV,
                              'VH':file_name_3bands_VH,'VHOld':file_name_3bands_VH, 'VHSummer':file_name_3bands_VH,
                              'Slope': file_slope, 'DEM': file_DEM, 'Aspect': file_aspect, 'VVmean':file_name_3bands_VV,
                              'VHmean':file_name_3bands_VH, 'Slopemean':file_slope, 'Treecover':file_treecover,
                              'Watercover':file_watercover, 'Glaciercover':file_glaciercover}

        df = MMfeat.get_features_swiss_pixelwise(df, set_of_features, name_file_features, band_features, height, width, file_saving=None)
        df.to_csv(csv_features_positives_file)

        df_neg_small = df_neg.sample(frac=1)[:len(df)]
        df = []
        df_neg_small.index = range(len(df_neg_small))
        df_neg_small = MMfeat.get_features_swiss_pixelwise(df_neg_small, set_of_features, name_file_features, band_features, height, width, file_saving=None)
        df_neg_small.to_csv(csv_features_negatives_file)
        df_neg_small = []
        ### view features
        # plt.figure(figsize=(12, 8))
        # for i, f in enumerate(set_of_features):
        #     plt.subplot(4,4, i+1)
        #     plt.title(set_of_features[i], fontsize=12)
        #     plt.hist([df[f].values, df_neg_small[f].values],
        #              bins=50, color=['r' ,'b'], alpha=0.7, label=[ 'avalanche','no avalanche'])
        #     plt.xticks(fontsize=10)
        #     plt.yticks(fontsize=10)
        #
        # plt.legend(prop={'size': 10})
        # plt.savefig(folder_saving_img_positives + date_current + 'feature_distribution_3means_covers.png')


        print('   phase 2 done.')

print('phase 3 : separating test and train starting...')

df_total_alldates = []
for i, date_current in enumerate(dates_current):
    csv_features_positives_file  = folder_saving_img_positives + date_current + '_features_3means_covers.csv'
    csv_features_negatives_file = folder_saving_img_negatives + date_current + '_features_3means_covers.csv'
    df = pd.read_csv(csv_features_positives_file).drop(['Unnamed: 0'],1)
    df_neg_small = pd.read_csv(csv_features_negatives_file).drop('Unnamed: 0',1)
    # add label
    df['label'] = np.ones([len(df),1])
    df_neg_small['label'] = np.zeros([len(df),1])
    df_total = pd.concat([df, df_neg_small], sort=False)
    # add date
    df_total['date'] = date_current
    if i==0:
        df_total_alldates = df_total
    else:
        df_total_alldates = pd.concat([df_total_alldates, df_total])

group = np.array(np.round(df_total_alldates.y /100))
y = df_total_alldates.label
X = df_total_alldates.drop(columns=['label']) #, 'x', 'y'])
df_total_alldates = []

# shuffle your data prior to separate train, test, ect.
X, y, group = shuffle(X, y, group, random_state=4)
X.index = range(len(X))
y.index = range(len(y))
#
gkf = GroupKFold(n_splits=4).split(X, y, group)
train_index, test_index = next(gkf)
X_train, X_test = X.iloc[train_index], X.iloc[test_index]
y_train, y_test = y.iloc[train_index], y.iloc[test_index]
X_train.index = range(len(X_train))
X_test.index = range(len(X_test))
y_train.index = range(len(y_train))
y_test.index = range(len(y_test))

dataset = X_train
dataset.loc[:, 'label'] = y_train
dataset_test = X_test
dataset_test.loc[:, 'label'] = y_test

print('Lengths of datasets: train, test:')
print(len(dataset), len(dataset_test))
folder_database = folder_learning + orbit + '_' + 'dataset_alldates/'
print('Writing train file for ' + tuile + ' in ' + folder_database)
MMpf.MakeDir(folder_database)
dataset.to_csv(folder_database + 'dataset' + '.csv')
dataset_test.to_csv(folder_database + 'dataset_test' + '.csv')

print('phase 3 done.')

print('phase 4: learning random forests starting...')

name_classifier = "RandomForest"

y = dataset.label
X = dataset[set_of_features]
y_test = dataset_test.label
X_test = dataset_test[set_of_features]

n_splits = 3
gkf = list(KFold(n_splits=n_splits).split(X, y))

folder_results = folder_learning + orbit + '_results_alldates/'
MMpf.MakeDir(folder_results)


clf = RandomForestClassifier(max_depth=10, n_estimators=128)
scores = cross_val_score(clf, X, y, cv=gkf)
avg_score = np.mean(scores)
std_dev = np.std(scores)

f = open(folder_results + 'results.txt', 'w')
print('mean score on train data =' + str(avg_score))
f.write('mean score on train data =' + str(avg_score) + '\n')
clf.fit(X,y)

with open(folder_results + 'learned_classifier.pkl', 'wb') as fid:
    pkl.dump(clf, fid)

pred_y_test = clf.predict(X_test)
dataset_test['predicted_label'] = pred_y_test
test_score = clf.score(X_test, y_test)
print('score on test data =' + str(test_score))
f.write('score on test data =' + str(test_score) + '\n')
#filename_predictions = 'dataset_test_with_predictions_' + orbit + '.csv'
#dataset_test_selected_cols = dataset_test['predicted_label']
#dataset_test_selected_cols.to_csv(folder_results + filename_predictions)
# get feature importance
f.write('features importances:')
for feat, nb in zip(set_of_features, clf.feature_importances_):
    f.write(feat + ' ' + str(nb) + '\n')
f.close()

print('phase 4 done.')
print('All good!')