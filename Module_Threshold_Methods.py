import numpy as np
import copy
import pandas as pd
from scipy import ndimage
from skimage import measure
import MMrasterio



def separate_insee_and_no_insee_detections(img_thresh, file_insee_tif):

    img_insee = MMrasterio.read_tif_to_np_array(file_insee_tif)
    img_thresh_insee = img_thresh * (img_insee > 0)
    img_thresh_no_insee = img_thresh * (img_insee == 0)
    return img_thresh_insee, img_thresh_no_insee


def get_adjascent_zones_thresholded(img_thresh_insee, img_thresh_noinsee, min_struct_to_keep=None,
                                    min_struct_to_keep_GT=None):
    '''
    with Swiss dataset
     !! in case you get memory error, please consider importing file names and open the 2nd once the 1st is not used anymore.
    :param img_thresh_insee:
    :param img_thresh_noinsee:
    :param min_struct_to_keep
    :return: tif binary image. 1 are the thresholded zones that are inside or attached to the corridor polygons (shapefile)
    '''

    if min_struct_to_keep_GT is not None:
        img_thresh_insee = ndimage.binary_erosion(img_thresh_insee, structure=min_struct_to_keep_GT).astype(float)
    if min_struct_to_keep is not None:
        img_thresh_noinsee_erosion = ndimage.binary_erosion(img_thresh_noinsee, structure=min_struct_to_keep).astype(
            float)
        img_thresh_noinsee = ndimage.binary_propagation(img_thresh_noinsee_erosion,
                                                             mask=img_thresh_noinsee).astype(float)
    img_thresh_noinsee = measure.label(img_thresh_noinsee)
    img_labels = img_thresh_insee * img_thresh_noinsee
    img_thresh_insee = []
    labels_to_keep = np.unique(img_labels)[1:].astype(int)  # !! only if a zero in the corner (should be the case)
    img_labels = []
    img_thresh_noinsee = img_thresh_noinsee.astype(int)
    img_thresh_noinsee = np.isin(img_thresh_noinsee, labels_to_keep)
    img_thresh_noinsee = 1. * img_thresh_noinsee

    return img_thresh_noinsee


def get_img_diff_of_adjascent_zones(img_thresh_noinsee= None, file_threshold_noinsee=None, img_thresh_adj=None,
                                    file_img_thresh_adj=None, min_struct_to_keep=None):
    if img_thresh_noinsee is None and file_threshold_noinsee is not None:
        img_thresh_noinsee = MMrasterio.read_tif_to_np_array(file_threshold_noinsee)
    if img_thresh_adj is None and file_img_thresh_adj is not None:
        img_thresh_adj = MMrasterio.read_tif_to_np_array(file_img_thresh_adj)
    img_thresh_noinsee_diff = img_thresh_noinsee * (1 - img_thresh_adj)
    if min_struct_to_keep is None:
        img_thresh_noinsee_diff_final = img_thresh_noinsee_diff
    else:
        img_thresh_noinsee_diff_erosion = ndimage.binary_erosion(img_thresh_noinsee_diff, structure=min_struct_to_keep).astype(float)
        img_thresh_noinsee_diff_final = ndimage.binary_propagation(img_thresh_noinsee_diff_erosion,
                                                        mask=img_thresh_noinsee_diff).astype(float)

    return img_thresh_noinsee_diff_final


def get_df_indices_nonzero_from_tif(tif_img=None, tif_file=None, saving_file=None, height_limit=0, width_limit=0):

    if tif_img is None and tif_file is not None:
        tif_img = MMrasterio.read_tif_to_np_array(tif_file)
    height = tif_img.shape[0]
    width = tif_img.shape[1]
    indexes = [[]]
    for i in range(height_limit, height):
        if np.sum(np.abs(tif_img[i, :])) == 0:
            continue
        for j in range(width_limit, width):
            if tif_img[i, j] != 0:
                indexes.append([i, j])
    tif_img = []
    indexes.pop(0)
    df = pd.DataFrame(indexes)
    df.columns = ['x', 'y']
    if saving_file is not None:
        df.to_csv(saving_file)
    return df


def remove_samples_outside_mask(img_thresh, file_mask):

    mask = MMrasterio.read_tif_to_np_array(file_mask)
    img_thresh = img_thresh * mask
    return img_thresh
