import glob
import numpy as np
import os
import sys
import pandas as pd
import pickle as pkl
import matplotlib.pyplot as plt

import MMrasterio
import Module_ProcessFiles as MMpf
import Module_load_features as MMfeat
import Module_Threshold_Methods as MMthresh


path = '/home/giffards/Documents/AvalancheProject/'
path_data = path + 'Data/Suisse/'
path_process = path + 'InputProcessing/Suisse/'

tuile = '32TLS'
orbit = 'A88' # 'D139' # for 32TLS

set_of_features = ['VV', 'VVOld', 'VVSummer', 'VH', 'VHOld', 'VHSummer', 'Slope', 'DEM', 'VVmean', 'VHmean',
                   'Slopemean','Aspect', 'Treecover']  # 'Watercover', 'Glaciercover'
min_struct_to_keep = None # can be np.ones(4, 4) SAR min structure
min_struct_to_keep_GT = np.ones((4, 4)) # SPOT min structure

folder_data = path_data + 'SAR/' + orbit + '/' + tuile + '/'
file_insee_tif = path_process + tuile + '/insee_zone_map_new.tif'
file_mask = path_process + tuile + '/'+ tuile + '_region_mask.tif'

folder_depots = path_process + tuile + '/Depots_refmoy/' # folder of the existing depots files of type ' '*' + orbit_large + '*' + date_current + '*' + 'VH' + '*'
folder_process = path + 'InputProcessing/Suisse/'+ tuile + '/3bands_log_tifs/' + orbit + '/' # path where the 3bands tiffs are of type tuile + '_' + orbit_large +
                                                                                                # '_RGB_VH_' + date_old + '_' + date_current + '_'+  'meanref' + '_log10.tif'
folder_results_all =  './results_32TLS_using_training_on_32TMS/'

file_savedmodel = './results_32TMS/RF_model/'+ 'A15' + '_results_alldates/learned_classifier.pkl'

file_slope = folder_data + 'Aux/' + tuile + '_PENTE.tif'
file_DEM = folder_data + 'Aux/' + tuile + '_DEM.tif'
file_aspect = folder_data + 'Aux/' + tuile + '_EXPO.tif'
file_treecover = folder_data + 'Aux/' + tuile + '_TREECOVER.tif'
file_watercover = folder_data + 'Aux/' + tuile + '_WATERCOVER.tif'
file_glaciercover = folder_data + 'Aux/' + tuile + '_GLACIERCOVER.tif'

########################################################################################
if orbit == 'D139':
    orbit_large = 'DES_139'
    dates_current = ['20180127', '20180121', '20180115', '20180109']
    dates_old = ['20180121', '20180115', '20180109', '20180103']
elif orbit == 'A88':
    orbit_large = 'ASC_088'
    dates_current = ['20180129', '20180123', '20180117', '20180111']
    dates_old = ['20180123', '20180117', '20180111', '20180105']

folder_saving_img_positives = folder_results_all + 'imgs_for_positives_VH' +'_' + orbit + '/'
folder_saving_img_negatives = folder_results_all + 'imgs_for_negatives_VH' +'_' + orbit + '/'
MMpf.MakeDir(folder_saving_img_positives)
MMpf.MakeDir(folder_saving_img_negatives)

band_features = {'VV': 2, 'VVOld': 1, 'VVSummer': 3,
                 'VH': 2, 'VHOld': 1, 'VHSummer': 3,
                 'Slope': 1, 'DEM': 1, 'Aspect': 1,
                 'VVmean': 2, 'VHmean': 2, 'Slopemean': 1,
                 'Glaciercover': 1, 'Treecover': 1, 'Watercover': 1}

print('orbit: ' + orbit)


for date_current, date_old in zip(dates_current, dates_old):
    try:
        file_threshold = glob.glob(folder_depots + '*' + orbit_large + '*' + date_current + '*' + 'VH' + '*')[0]
    except IndexError:
        sys.exit(
            'Error! no folder or file found:  ' + folder_depots + '*' + orbit_large + '*' + date_current + '*' + 'VH' + '*')

    print(date_current)
    ############################################
    print('   phase 1: tif images of positive and negative samples starting...')
    img_thresh_noinsee = MMrasterio.open_tif(file_threshold)
    crs = MMrasterio.get_tiff_crs(img_thresh_noinsee)
    transform = MMrasterio.get_tiff_transform(img_thresh_noinsee)
    img_thresh_noinsee = img_thresh_noinsee.read()[0]

    img_thresh_noinsee = MMthresh.remove_samples_outside_mask(img_thresh_noinsee, file_mask)

    img_thresh_insee, bla = MMthresh.separate_insee_and_no_insee_detections(img_thresh_noinsee, file_insee_tif)
    bla = []

    img_thresh_adj = MMthresh.get_adjascent_zones_thresholded(img_thresh_insee, img_thresh_noinsee,
                                                              min_struct_to_keep=min_struct_to_keep,
                                                              min_struct_to_keep_GT=min_struct_to_keep_GT)
    img_thresh_insee = []
    img_thresh_noinsee_diff = MMthresh.get_img_diff_of_adjascent_zones(img_thresh_noinsee,
                                                                          img_thresh_adj=img_thresh_adj, file_img_thresh_adj=None,
                                                                          min_struct_to_keep=min_struct_to_keep)

    filesaving_positives = folder_saving_img_positives + date_current + '.tif'
    filesaving_negatives = folder_saving_img_negatives + date_current + '.tif'

    MMrasterio.save_array_to_tiff(img_thresh_adj, transform, filesaving_positives, crs)
    img_thresh_adj = []
    MMrasterio.save_array_to_tiff(img_thresh_noinsee_diff, transform, filesaving_negatives, crs)
    img_thresh_noinsee_diff = []

    print('   phase 1 done.')
    ############################################
    print('   phase 2: writing features starting...')

    file_name_3bands_VH = folder_process + tuile + '_' + orbit_large + '_RGB_VH_' + date_old + '_' + date_current + '_'+ \
                 'meanref' + '_log10.tif'
    file_name_3bands_VV = folder_process + tuile + '_' + orbit_large + '_RGB_VV_' + date_old + '_' + date_current + '_'+ \
                 'meanref' + '_log10.tif'

    file_threshold_positive = folder_saving_img_positives + date_current + '.tif'
    file_threshold_negative = folder_saving_img_negatives + date_current + '.tif'

    img_positives, crs, transform = MMrasterio.open_tif_crs_transform(file_threshold_positive)
    img_positives = img_positives.read()[0]

    #img_insee = MMrasterio.read_tif_to_np_array(file_insee_tif)
     ## get indexes of positive and negative samples
    height = img_positives.shape[0]
    width = img_positives.shape[1]

    df = MMthresh.get_df_indices_nonzero_from_tif(tif_img=img_positives, tif_file=None,
                                                      saving_file=folder_saving_img_positives + date_current + '_indices.csv')
    img_positives = []
    df_neg = MMthresh.get_df_indices_nonzero_from_tif(tif_img=None, tif_file=file_threshold_negative,
                                                      saving_file=folder_saving_img_negatives + date_current + '_indices.csv')

    ## get the features
    csv_features_positives_file = folder_saving_img_positives + date_current + '_features_3means_covers.csv'
    csv_features_negatives_file = folder_saving_img_negatives + date_current + '_features_3means_covers.csv'
    name_file_features = {'VV':file_name_3bands_VV, 'VVOld':file_name_3bands_VV, 'VVSummer': file_name_3bands_VV,
                          'VH':file_name_3bands_VH,'VHOld':file_name_3bands_VH, 'VHSummer':file_name_3bands_VH,
                          'Slope': file_slope, 'DEM': file_DEM, 'Aspect': file_aspect, 'VVmean':file_name_3bands_VV,
                          'VHmean':file_name_3bands_VH, 'Slopemean':file_slope, 'Treecover':file_treecover,
                          'Watercover':file_watercover, 'Glaciercover':file_glaciercover}

    df = MMfeat.get_features_swiss_pixelwise(df, set_of_features, name_file_features, band_features, height, width, file_saving=None)
    df.to_csv(csv_features_positives_file)

    df_neg = MMfeat.get_features_swiss_pixelwise(df_neg, set_of_features, name_file_features, band_features, height, width, file_saving=None)
    df_neg.to_csv(csv_features_negatives_file)

    ### view features
    plt.figure(figsize=(12, 8))
    for i, f in enumerate(set_of_features):
        plt.subplot(4,4, i+1)
        plt.title(set_of_features[i], fontsize=12)
        plt.hist([df[f].values, df_neg[f].values],
                 bins=50, color=['r' ,'b'], alpha=0.7, label=['avalanche','no avalanche'])
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
    plt.legend(prop={'size': 10})
    plt.savefig(folder_saving_img_positives + date_current + 'feature_distribution.png')
    df_neg =[]
    df = []
    print('   phase 2 done.')
    #############################
    print('   phase 3: saving test starting...')
    folder_results = folder_results_all + date_current + '/'
    MMpf.MakeDir(folder_results)

    df = pd.read_csv(csv_features_positives_file).drop(['Unnamed: 0'],1)
    df_neg = pd.read_csv(csv_features_negatives_file).drop('Unnamed: 0',1)

    df['label'] = np.ones([len(df), 1])
    df_neg['label'] = np.zeros([len(df_neg), 1])
    df_total = pd.concat([df, df_neg], sort=False)

    dataset_test = df_total
    dataset_test.index = range(len(dataset_test))

    print('Lengths of dataset: test:')
    print(len(dataset_test))

    print('Writing test file for ' + tuile + ' in ' + folder_results)
    MMpf.MakeDir(folder_results)
    dataset_test.to_csv(folder_results + '/dataset_test_all' + '.csv')

    print('   phase 3 done.')
    #########################################
    print('   phase 4: prediction using random forests starting...')

    y_test = dataset_test.label
    X_test = dataset_test[set_of_features]


    filename_predictions = 'dataset_test_with_predictions.csv'

    with open(file_savedmodel, 'rb') as fid:
        clf = pkl.load(fid)

    f = open(folder_results + 'results.txt', 'a')
    selected_cols = ["x", "y", "label"] #, "insee", "label", "date"]
    if 'date' in dataset_test.keys():
        selected_cols.append('date')
    if hasattr(clf, 'predict_proba'):
        pred_y_test = clf.predict_proba(X_test)[:, 1]
    else:
        pred_y_test = clf.predict(X_test)
    dataset_test['predicted_label'] = pred_y_test
    selected_cols.append('predicted_label')
    f.write('score on test data ' + '=' + str(clf.score(X_test, y_test)) + '\n')
    f.close()
    dataset_test_selected_cols = dataset_test[selected_cols]
    print('Writing predictions file on test data')
    dataset_test_selected_cols.to_csv(folder_results + filename_predictions)

    x_coord = dataset_test.x
    y_coord = dataset_test.y
    y_true = dataset_test.label
    y_true2 = y_true.__deepcopy__()
    for i in range(len(y_true)):
        if y_true[i] == 0:
            y_true2[i] = -1
    y_true = y_true2

    img_label_corrs = MMrasterio.open_tif(file_insee_tif)
    crs = MMrasterio.get_tiff_crs(img_label_corrs)
    transform = MMrasterio.get_tiff_transform(img_label_corrs)

    prediction_date_pred = np.zeros([img_label_corrs.height, img_label_corrs.width])
    prediction_date_true = np.zeros([img_label_corrs.height, img_label_corrs.width])
    for ind in dataset_test.index:
        i = int(x_coord[ind])
        j = int(y_coord[ind])
        prediction_date_pred[i, j] = dataset_test['predicted_label'][ind]
        prediction_date_true[i, j] = int(y_true[ind])

    file_saving_pred = folder_results + 'pred_alltest_proba.tif'
    file_saving_truth = folder_results + 'truth_from_SPOT.tif'
    print('   Writing file: ' +  'pred_alltest_proba.tif')
    MMrasterio.save_array_to_tiff(prediction_date_pred, transform, file_saving_pred, crs)
    MMrasterio.save_array_to_tiff(prediction_date_true, transform, file_saving_truth, crs)

    print('   phase 4 done.')

print('All good!')
