import os


def MakeDir(folderSaving):
    try:
        os.makedirs(folderSaving)
    except OSError:
        if not os.path.isdir(folderSaving):
            raise


