import numpy as np
import pandas as pd
import rasterio
import copy
from scipy import signal
from random import shuffle, sample
import random
from datetime import datetime
import MMrasterio

def get_features_swiss_pixelwise(df, set_of_features, name_file_features, band_features, height, width, file_saving = None):
    w_mean = np.ones([5, 5]) / 25

    for f in set_of_features:
        print(f)
        img_rasterio = MMrasterio.open_tif(name_file_features[f])
        mat = img_rasterio.read(band_features[f], out_shape=(1, height, width))
        if f[-4:] == 'mean':
            mat = signal.convolve2d(mat, w_mean, 'same')
        feats_col = np.zeros([len(df), 1])
        for index, row in df.iterrows():
            feats_col[index] = mat[int(row.x), int(row.y)]
        df[f] = feats_col
        if f in ['Aspect', 'Slope', 'DEM', 'SlopeMean']:
            df[f][df[f] < 0] = 0
    # df.Aspect[df.Aspect < 0] = 0
    # df.Slope[df.Slope < 0] = 0
    # df.DEM[df.DEM < 0] = 0
    if file_saving is not None:
        df.to_csv(file_saving)
    return df
