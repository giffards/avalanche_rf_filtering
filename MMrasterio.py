# Module to read and process tiff images
import numpy as np
import rasterio
import rasterio.plot as rioplot


def open_tif(name_file):
    '''
    :param name_file: name of the tif file to open
    :return: rasterio.io.DatasetReader
    '''
    rasterio_img = rasterio.open(name_file)
    return rasterio_img


def open_tif_crs_transform(name_file):
    rasterio_img = rasterio.open(name_file)
    crs =get_tiff_crs(rasterio_img)
    transform = get_tiff_transform(rasterio_img)
    return rasterio_img, crs, transform


def read_tif_to_np_array(tif_file):
    '''
    :param tif_file_for_single_date: tif file with labels for one date
    :return: converted numpy array of the tif image
    '''

    img = open_tif(tif_file)
    img_array = img.read()[0]
    return img_array

def get_tif_nb_bands(rasterio_img):
    '''
    get the number of bands (or layers) of a tif image
    :param rasterio_img: rasterio.io.DatasetReader or rasterio.io.DatasetWriter
    :return: int
    '''
    return rasterio_img.count


def get_tif_band(rasterio_img, num_band):
    '''
    get one of the bands of the tif image as a numpy array
    :param rasterio_img: rasterio.io.DatasetReader or rasterio.io.DatasetWriter
    :param num_band: int from 1 to get_tif_nb_bands(rasterio_img)
    :return: np.array
    '''

    if 0 < num_band <= get_tif_nb_bands(rasterio_img):
        mat = rasterio_img.read(num_band)
    else:
        print('Error. The band does not exist in this rasterio image.')
        raise IOError
    return mat


def get_tiff_transform(rasterio_img):
    '''
    get the transformation object of a tiff image (offset rotation...)
    :param rasterio_img: rasterio.io.DatasetReader or rasterio.io.DatasetWriter
    :return: transform affine.Affine (or else?)
    '''
    transform = rasterio_img.meta['transform']
    return transform

def get_tiff_crs(rasterio_img):
    crs = rasterio_img.meta['crs']
    return crs


def plot_img(rasterio_img):
    rioplot.show(rasterio_img, with_bounds=True)
    return 0


def save_array_to_tiff(mat, transform, filesaving, crs='+proj=latlong'):
    '''
    save a numpy array to a rasterio tiff image
    :param mat: matrix array of size (template.height, template.width)
    :param transform: a rasterio transform object (affine.Affine for ex.) Use
    :param filesaving: path + name + '.tiff'
    :return: 0
    '''
    new_dataset = rasterio.open(filesaving, 'w', driver='GTiff', height=mat.shape[0],
                                width=mat.shape[1], count=1, dtype=mat.dtype,
                                crs=crs, transform=transform)
    new_dataset.write(mat, 1)
    new_dataset.close()
    return 0


def save_multiple_arrays_to_tiff(mats, transform, filesaving, crs='+proj=latlong'):
    '''
    save a list of numpy arrays to a rasterio tiff image
    :param mats: list of matrix arrays of size (template.height, template.width)
    :param transform: a rasterio transform object (affine.Affine for ex.) Use
    :param filesaving: path + name + '.tiff'
    :return: 0
    '''
    new_dataset = rasterio.open(filesaving, 'w', driver='GTiff', height=mats[0].shape[0],
                                width=mats[0].shape[1], count=len(mats), dtype=mats[0].dtype,
                                crs=crs, transform=transform)
    for i, mat in enumerate(mats):
        new_dataset.write_band(i+1, mat)
    new_dataset.close()
    return 0


def save_3bands_log10_to_tiff(red_tif_name, geen_tif_name, blue_tif_name, band_number=1, output_tif_name='output.tif'):
    '''

    :param red_tif_name: str, full path
    :param geen_tif_name: str. full path
    :param blue_tif_name: str, full path
    :param band_number: (int) the number of the band to get from the tif (1 is VV, 2 is VH)
    :param output_tif_name: saving path name
    :return: 0
    '''
    print('Processing red band..')
    band_rasterio_old = open_tif(red_tif_name)
    tif_mat = get_tif_band(band_rasterio_old, band_number)
    log_mat_old = np.where(tif_mat > 0, 10*np.log10(tif_mat), 0)

    print('Processing green band..')
    band_rasterio = open_tif(geen_tif_name)
    tif_mat = get_tif_band(band_rasterio, band_number)
    log_mat = np.where(tif_mat > 0, 10*np.log10(tif_mat), 0)

    print('Processing blue band..')
    band_rasterio_summer = open_tif(blue_tif_name)
    tif_mat = get_tif_band(band_rasterio_summer, band_number)
    log_mat_summer = np.where(tif_mat > 0, 10*np.log10(tif_mat), 0)

    crs = get_tiff_crs(band_rasterio)
    transform = get_tiff_transform(band_rasterio)

    save_multiple_arrays_to_tiff([log_mat_old, log_mat, log_mat_summer], transform, output_tif_name, crs)

    return 0
