**Avalanche Random Forest filtering for SAR detection**

For the Swiss data.

**training the model** 

use script `training_RF_model_script.py`

 4 steps :  1. creating tif images of positive and negative samples
            2. writing the features in csv (half of positives, half of negatives)
            3. creating dataset and separating test and train
            4. learning the random forest model (fitting)

**predicting from the model** 
use script `predict_RF_model_script.py`
Same steps, but in 2. we take all of the samples and in 4. we use the model estimated by the training.


**Inputs needed**
- file_insee_tif: Tif file of the zone where nonzero elements are the 'ground truth' (validated avalanches)
- file_mask: Tif of the mask of the zone of study (white only where the ground truth was performed)
- folder_depots: folder of the existing detected deposit files of type: '*'+orbit_large+'*'+date_current+'*'+'VH'+'*' (ex. s1a_sig_32TLS_vvvh_ASC_088_20180111t172315_filteredREFmoyT2_DEPOT_VH.tif)
- folder_process: path where the 3bands tiffs are of type: tuile+'_'+orbit_large+'_RGB_VH_'+date_old+'_'+date_current+'_'+'meanref'+'_log10.tif'
- tif files of the different features: file_slope, file_DEM, file_aspect, file_treecover, (file_watercover, file_glaciercover)
- dates of the studied orbit passages (dates_current) and their corresponding previous passages (dates_old)
- set_of_features: list of the features wanted to be used in the learning

- file_savedmodel: for the predict part only, the path to the RF model created (learned_classifier.pkl)

**Ouputs**

*training:*
 - results_zoneoftraining/ (ex. 32TMS) with:
    - imgs_for_negatives.../ and imgs_for_positives.../ containing the tif files of positives and negatives samples, and also the features csv files.
    - RF_model/ with the dataset and the results folder. The trained RF model is stored in this results/ folder.

*predicting:*
- results_zoneofprediction_using_training_on_zoneoftraining/ (ex. 32TLS and 32TMS) with:
    - same imgs_for_negatives.../ positives.../ folders. The only difference are the negative samples, where here we keep them all.
    - 20180111/ (or other dates) : results for every date.
    

